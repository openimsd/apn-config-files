<?php
echo "APN to CFG 0.0.0.1\n";
$input_file = NULL;
$output_dir = NULL;
$commands = array();
if (!isset($argc) || $argc < 2) {
    echo "Missing arguments!\n";
    echo "Usage:\n";
    echo "idl2json INPUT_FILE OUTPUT_DIR\n";
    exit();
}
if (isset($argv[1])) {
    $input_file = $argv[1];
}
if (isset($argv[2])) {
    $output_dir  = $argv[2];
}

$file_in_data = file_get_contents($input_file);
if (!$file_in_data) {
    echo "ERROR: Couldn't open the file!\n";
    exit();
}
$input_data = simplexml_load_string($file_in_data);
if (!$input_data) {
    echo "ERROR: Can't parse JSON data\n";
    exit();
}
$input_data = json_decode(json_encode($input_data), true);
foreach ($input_data['apn'] as $key => $ritem) {
    if (isset($ritem['@attributes'])) {
        $item = $ritem['@attributes'];
        if (isset($item['type']) && strpos($item['type'], 'ims') !== FALSE) {
            $outstr = '';
            $outfile = $output_dir . '/' . $item['mcc'] . '-' . $item['mnc'] . '.ini';
            if (isset($item['carrier']))
                $outstr .= 'carrier=' . $item['carrier'] . "\n";
            if (isset($item['apn']))
                $outstr .= 'apn=' . $item['apn'] . "\n";
            if (isset($item['mcc']))
                $outstr .= 'mcc=' . $item['mcc'] . "\n";
            if (isset($item['mnc']))
                $outstr .= 'mnc=' . $item['mnc'] . "\n";
            if (isset($item['user']))
                $outstr .= 'user=' . $item['user'] . "\n";
            if (isset($item['password']))
                $outstr .= 'password=' . $item['password'] . "\n";
            if (isset($item['proxy']))
                $outstr .= 'proxy=' . $item['proxy'] . "\n";
            if (isset($item['port']))
                $outstr .= 'port=' . $item['port'] . "\n";
            if (isset($item['mtu']))
                $outstr .= 'mtu=' . $item['mtu'] . "\n";
            if (isset($item['type']))
                $outstr .= 'type=' . $item['type'] . "\n";
            if (isset($item['protocol']))
                $outstr .= 'protocol=' . $item['protocol'] . "\n";
            if (isset($item['roaming_protocol']))
                $outstr .= 'roaming_protocol=' . $item['roaming_protocol'] . "\n";
            if (isset($item['bearer_bitmask']))
                $outstr .= 'bearer_bitmask=' . $item['bearer_bitmask'] . "\n";
            if (isset($item['profile_id']))
                $outstr .= 'profile_id=' . $item['profile_id'] . "\n";
            if (isset($item['modem_cognitive']))
                $outstr .= 'modem_cognitive=' . $item['modem_cognitive'] . "\n";
            if (isset($item['max_conns']))
                $outstr .= 'max_conns=' . $item['max_conns'] . "\n";
            if (isset($item['max_conns_time']))
                $outstr .= 'max_conns_time=' . $item['max_conns_time'] . "\n";

            file_put_contents($outfile, $outstr);
        }
    } else {
        echo "Item doesnt have attribs\n";
    }
}



//print_r($input_data);
exit();
